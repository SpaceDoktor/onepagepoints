
### General Principles

* **The most important rule:** Whenever the rules are
unclear or don't seem quite right, use common
sense and personal preference. Have fun!
* **Quality Tests:** Whenever you must take a quality
test roll one six-sided die trying to score the unit's
quality value or higher, which counts as a success.
* **Modifiers:** If you need to roll a 7+ or higher to
succeed, then a roll of 6+ followed by 4+ counts
as a success. Note that a roll of 1 is always a fail.
* **Line of Sight:** If you can draw a straight line from
attacker to target without passing through any
obstacle or unit, then it has line of sight.
Preparation
* **The Battlefield:** The game is played on a flat 6'x4'
surface, with at least 5-10 pieces of terrain on it.
* **The Armies:** Before the game players must put
together a force of 500pts worth of units and
upgrades, with at least 1 Hero unit and up to 2
Vehicles or Monsters units.
* **Deployment:** Players roll-off, and the winner
picks which long table edge is his deployment
zone, with his opponent taking the opposite edge.
Then the players alternate in placing one unit
each within 12" of their table edge, starting with
the player that won the deployment roll-off.
* **Mission:** Place D3+2 objective markers on the
table. The players roll-off, and the winner picks
who places the first objective marker. Then the
players alternate in placing one marker each
outside of the deployment zones, and over 9"
away from other markers. Units that are within 3"
of a marker whilst no enemy unit is count as
having seized it, however Pinned units can't seize
or stop other units from seizing markers. After 4
rounds the player that managed to seize most
markers wins.

### Preparation

* **The Battlefield:** The game is played on a flat 6'x4'
surface, with at least 5-10 pieces of terrain on it.
* **The Armies:** Before the game players must put
together a force of 500pts worth of units and
upgrades, with at least 1 Hero unit and up to 2
Vehicles or Monsters units.
* **Deployment:** Players roll-off, and the winner
picks which long table edge is his deployment
zone, with his opponent taking the opposite edge.
Then the players alternate in placing one unit
each within 12" of their table edge, starting with
the player that won the deployment roll-off.
* **Mission:** Place D3+2 objective markers on the
table. The players roll-off, and the winner picks
who places the first objective marker. Then the
players alternate in placing one marker each
outside of the deployment zones, and over 9"
away from other markers. Units that are within 3"
of a marker whilst no enemy unit is count as
having seized it, however Pinned units can't seize
or stop other units from seizing markers. After 4
rounds the player that managed to seize most
markers wins.

### Playing the Game

The game is played in rounds in which players
alternate in activating one unit each until all units
have been activated. The player that deployed
first starts activating first on the first round. Each
new round the player that finished activating first
in the previous round gets to go first.

### Activation

The player declares which unit he activates, and it
may do one of the following:

| Action | Move | Notes |
|---|---|---|
| Hold | 0" | May shoot. |
| Advance | speed | May shoot after moving. |
| Rush | 2x speed | May not shoot. |
| Charge | 2x speed | Move into melee. |

### Movement

All members of a unit must remain within 2" of at
least one other member and within 6" of all other
members at all times. Units may only move within
1" of other units when charging, and may only
use Charge actions if their move brings at least
one model in base contact with the target.

### Shooting

All models that are in range and have line of sight
to an enemy may fire all of their weapons at it.
Shooting models take one Quality test per attack
and each success is a hit. For each hit roll one die
trying to score the target's Defense value or
higher, and each success causes a wound. For each wound
one target model with the least remaining wounds loses
one wound. When it reaches 0 or less, the defender must
remove it.

### Melee

Charging models must move into base contact or
as close as possible with the enemy, and then
defenders do the same by moving by up to 3". All
attackers within 2" of an enemy may strike with
all melee weapons, which works like shooting.
Then any remaining defenders may strike back in
the same way. Once both sides have attacked the
unit that caused fewer wounds must take a
morale test and the attacker must move back by
1". If one of the two units is destroyed the other
may move by up to 3".

### Morale

Whenever a unit takes wounds which brings it
down to half or less of its total size or wounds
value, then it must take a morale test. The unit
takes a Quality test and if failed it is Pinned until
the end of its next activation. Pinned units have -1
to their attack rolls in melee and must spend their
next activation idle. Units in melee only take
morale tests if they caused fewer wounds, and if
they fail a morale test whilst down to half or less
of their total size or wounds value, then they Rout
(remove the unit from play).

### Terrain

* **Cover (forests, ruins, sandbags, etc.):** Units that
shoot at enemies with most models within or
behind cover terrain get -1 to their shooting rolls.
* **Difficult Terrain (woods, mud, rivers, etc.):** Units
moving through difficult terrain can't move more
than 6" at a time, regardless of action.
* **Dangerous Terrain (quicksand, razor wire, mine
fields, etc.):** Roll one die for every model that
moves across dangerous terrain or activates in it.
On a roll of 1 the model takes one wound.

### Unit Types

* **Heroes:** These models can be deployed as part of
other friendly Infantry units. When rolling for
Defense or Morale use the value that the majority
of models have.
* **Infantry:** Any unit that is not a Vehicle or Monster
counts as Infantry. You can deploy two copies of
the same unit as one big unit, however upgrades
that affect all models must be bought for both.
* **Vehicles/Monsters:** Always count as having the
Fear and Impact(3) special rules.
* **Titans:** Always deal 6 automatic hits with AP(2) in
melee and count as having the Fear special rule.
* **Flyers:** These units don't physically interact with
any other models or terrain and can't be moved
into base contact with. Non-flyer units targeting
them count as being an extra 12" away when
measuring and they get -1 to their shooting rolls.
When activated the model must move a full 18"
to 36" in a straight line, and if this puts it on top
of other units/terrain it must keep moving until it
has space. If this move brings it outside the table
then its activation ends and it may be placed back
on any table edge.

### Weapons

Weapon profiles are listed directly on the unit's
card and are represented like this:
? Name (Range, Attacks, Special Rules)
Weapons with a range value provide attack dice
to shooting, others to melee. Models without a
melee weapon always have 1 attack in melee.

### Special Rules

* **Ambush:** You may choose not to deploy this
model with your army, but instead keep it in
reserve. After round 1 roll one die at the
beginning of each round, and on a 4+ you may
place the model anywhere on the table over 6"
away from enemy units. Then roll one die, on a 5+
the opponent may move the model by up to 12"
(must be in a valid position). On the last round
this model arrives automatically.
* **Anti-Air:** This weapon gets +1 to its shooting rolls
against enemy Flyers.
* **AP(X):** When rolling to wound with this weapon
the target gets Defense -X.
* **Blast(X):** Multiply this weapon's hits by X.
* **Deadly:** Models taking wounds from this weapon
multiply the total by 3.
* **EMP:** When hitting Vehicles this weapon always
wounds on a roll of 2+.
* **Fast:** This model moves 9" when using Advance
and 18" when using Rush/Charge actions.
* **Fear:** Enemies without Fear must take a morale
test before fighting in melee with this model. If
the test is failed the unit gets -1 to its attack roll.
* **Fearless:** This model gets +1 to its morale rolls.
* **Flying:** This model may move through other units
and obstacles, and it may ignore terrain effects.
* **Furious:** This model takes one Quality test when
Charging, if successful it deals 1 automatic hit.
* **Impact(X):** This model deals X automatic hits
when using Charge actions.
* **Indirect:** This weapon may be fired at enemies
that are not within line of sight, however targets
not within line of sight count as being in cover.
* **Limited:** This weapon may only be used once.
* **Linked:** This weapon gets +1 to its attack rolls.
* **Poison(X):** When rolling to wound add +X to the
result. This rule does not affect Vehicles.
* **Psychic(X):** This unit may cast one spell at the
beginning of its activation. Roll X dice and use one
spell from its army that has a number in brackets
which is equal to or lower to the highest result.
* **Regeneration:** When this model takes wounds
roll one die for each, on a 5+ it is ignored.
* **Rending:** When rolling a result of 6 to hit with this
weapon it always wounds on a roll of 2+.
* **Scout:** This model is deployed after all other units
have been deployed. Scout units may be placed
anywhere over 12" away from enemies. If both
players have scout units the players must roll-off
to see who deploys first.
* **Slow:** This model moves 4" when using Advance
and 8" when using Rush/Charge actions.
* **Sniper:** Models firing this weapon count as having
Quality 2+ and ignore cover. The attacker may
pick which model from the target unit is hit.
* **Stealth:** Enemies get -1 to their shooting rolls
against this unit.
* **Strider:** This model can move more than 6" when
passing through difficult terrain.
* **Transport(X):** This model may transport up to X
Infantry models. Units may embark by moving
into contact with a transport and may use an
Advance action to disembark. Units may also be
deployed within a transport. If a unit is inside a
transport when it is destroyed it must take a
Dangerous Terrain test, is immediately Pinned,
and surviving models must be placed within 6" of
the transport before it is removed.

### Dispelling

When an enemy Psychic within 18" of one of your
Psychics tries to cast a spell, you may try to block it.
Roll as many dice as your Psychic's X value and pick
the highest result. If it is higher than the enemy
Psychic's highest result, then the spell is blocked
and its effects are not resolved. A Psychic that has
already cast spells during a round may not dispel,
and a Psychic that already dispelled during a round
may not cast spells.

### Missions

When using the following missions the table and
armies are set up as described in the core rules. You
can either choose any of these or play with a
random objective by rolling one die on this table:

Result | Mission
--- | ---
1 | Duel
2 | Seize Ground
3 | Relic Hunt
4 | Sabotage
5 | Breakthrough
6 | King of the Hill

* **Placing Objectives:** The players roll-off, and the
winner picks who places the first objective marker.
Then the players alternate in placing one marker
each outside of the deployment zones, and over 9"
away from other markers.
* **Seizing Objectives:** Units that are within 3" of a
marker whilst no enemy unit is also within 3" of the
same marker count as having seized it. Pinned units
can't seize or stop other units from seizing markers.
* **1 - Duel:** Place D3+2 markers. After 4 rounds the
player that has seized most markers wins.
* **2 - Seize Ground:** Place 4 markers, one in each
table quarter. After 4 rounds the player that has
seized most markers wins.
* **3 - Relic Hunt:** Place 3 markers. If a unit seizes a
marker it picks it up and if it is destroyed the marker
is dropped on the spot. After 4 rounds the player
that has seized most markers wins.
* **4 - Sabotage:** Place 2 markers, each one
belonging to a player. Seizing an enemy marker
destroys it. After 4 rounds the player that destroyed
the enemy marker whilst keeping his own marker
intact wins.
* **5 - Breakthrough:** Place 2 markers, one at the
center of each deployment zone. After 4 rounds the
player that has seized most markers wins.
* **6 - King of the Hill:** Place 1 marker at the center
of the table. After 4 rounds the player that has
seized the marker wins.
