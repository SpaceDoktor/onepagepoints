# OnePagePoints

Small python 3.7 script to calculate units/upgrades points and generate army books for [onepagerule Grimdark Future](https://onepagerules.com/)
Compared to original points system, this algorithm lower the weapon cost, and increase the cost of high defense, high toughness units.
It generates one html page, or one pdf file per faction which replace the army book.

# Lastest version is available as a gitlab pages

[https://kdj0c.gitlab.io/onepagepoints/](https://kdj0c.gitlab.io/onepagepoints/)

# Start Contributing

If you want to test a modification (let's say you want Assault Rifle to be AP2)
You can edit the file in gitlab:
https://gitlab.com/kdj0c/onepagepoints/edit/master/Common/equipments.yml

Add a new line with "ap: 2" for Assault Rifle, and Commit change.

Then create a mergerequest like this one :
[https://gitlab.com/kdj0c/onepagepoints/merge_requests/13](https://gitlab.com/kdj0c/onepagepoints/merge_requests/13)

Then at the bottom of the page, when the pipeline is finished (Green instead of blue)
Click on the pipeline tab (on the right of Discussions and Commits), and then on the download icon, on the right side of the pipeline, and you will get a zip file with an html file per faction that you can open in your browser.
You will see that for all factions, all Units/Upgrades will have an Assault Rifle with AP2, and their unit cost and upgrade cost will be higher (or lower when you replace Assault Rifle with another weapon).

Then if a maintainer (me) approve the mergerequest, and merge it, the updated files will be available on gitlab pages :
[https://kdj0c.gitlab.io/onepagepoints/](https://kdj0c.gitlab.io/onepagepoints/)

With this collaboration tool, anyone can propose some changes on weapons stats, units, add a new faction, or even adjust point calculation.

Currently, gitlab only generate html pages per faction, but it can also generate pdf, with a custom runner.

# Install on a local Computer

Tested only with python 3.7 on Archlinux. It should work everywhere you can run python/Latex

# Dependencies

* PyYAML to read yaml files with python
* xelatex with a few plugins (only if you want to generate pdf files, it's not needed for html)
* make to have easier command line option, but not mandatory

# Source File details

 * onepagepoints.py : library to calculate individual cost of weapons/units, also a main() to do unit tests
 * onepagebatch.py : script which read each faction .yml files (equipments.yml, faction.yml, units.yml, upgrades.yml), and generate .html, .tex, and .txt output.
 * indentyaml.py : script to indent and force format for all .yml files.
 * generate_faction.py : script that is only used once to create a new faction from a csv file
 * testpoints.py : a small pytest script, I didn't put much unit test here. It can be used to check for regression.
 * Template/header.html : html/css header to generate a cool html page.
 * Template/header.tex : LaTeX header file, which define all LaTeX macros which will be used to generate the pdf.
 * Makefile : simple script to generate all pdf at once !


# Make Commands

to build all factions pdf (they are generated in build/Faction.pdf):
$ `make -j4`

to build html files for all factions:
$ `make html`

to build only 'Tao' html and pdf:
$ `make Tao`

to indent all yaml files (mandatory before merging changes to yaml files):
$ `make indent`
