#!/usr/bin/env python3

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import copy

# Adjust defense and attack cost, to match onepagerules current prices
adjust_defense_cost = 0.7
adjust_attack_cost = 0.7


# Cost per defense point (2+ => 6, 6+ => 24, 10+ => 58)
def defense_cost(d):
    return (0.9 * d * d + d + 10.0) / 2.0


# defense cost multiplier (higher quality units are tougher, due to moral test)
# 1 for 2+ quality, 0.6 for 6+ quality
def quality_defense_factor(q):
    return 1.0 - 0.1 * (q - 2.0)


# Attack cost multiplier, probability to hit according to quality
# 5/6 for 2+, 1/6 for 6+
def quality_attack_factor(q):
    return (7.0 - q) / 6.0


# AP cost multiplier.
# 1 for no AP, * 1.2 for each AP point
def ap_cost(ap):
    return (1.2 ** ap)


# Range cost multiplier.
# melee threaten range is charge distance (12" = 2 * speed)
# guns threaten range is advance distance (6" = speed) + weapon range
def range_cost(wrange, speed):
    if wrange == 0:
        c = (2 * speed) ** 0.75
    else:
        c = (wrange + speed) ** 0.75
    return c


# Handle D3+1 or 2D6+2 values
# return the mean to calculate the cost
def dice_mean(value):
    if isinstance(value, int):
        return value

    n, rem = value.split('D')
    if '+' in rem:
        sides, add = rem.split('+')
    else:
        sides, add = rem, 0

    mean = (int(sides) + 1) / 2.0
    if n:
        mean *= int(n)
    return mean + int(add)


# WargGear can include special rules for model, and weapons
# Like a jetbike gives "fast" rules and a Linked ShardGun
class WarGear:
    def __init__(self, name='Unknown Gear', special=[], weapons=[], text=''):
        self.name = name
        self.specialRules = special
        self.weapons = weapons
        self.text = text

    @classmethod
    def from_dict(self, name, data, armory):
        data['weapons'] = armory.get(data.get('weapons', []))
        return self(name, **data)

    def Profile(self):
        if self.text:
            return '(' + self.text + ')'
        else:
            return '(' + ', '.join(self.specialRules + [str(w) for w in self.weapons]) + ')'

    def __str__(self):
        return self.name + ' ' + self.Profile()

    def Cost(self, speed, quality):
        return sum([w.Cost(speed, quality) for w in self.weapons])


# Class for weapons
class Weapon:
    def __init__(self, name='Unknown Weapon', range=0, attacks=0, ap=0, special=[]):
        self.name = name
        self.range = range
        self.attacks = attacks
        self.armorPiercing = ap
        self.weaponRules = special
        self.specialRules = []

    def __repr__(self):
        return "{0}({1})".format(self.name, self.__dict__)

    def Profile(self):
        def fmtnz(value, fmt):
            if value:
                return [fmt.format(value)]
            return []

        prof = fmtnz(self.range, '{0}"') + ['A{0}'.format(self.attacks)] + fmtnz(self.armorPiercing, 'AP({0})')
        # Remove "Linked" special rule if the name contain "Linked"
        prof += [wr for wr in self.weaponRules if wr != 'Linked' or 'Linked' not in self.name.split()]
        return '(' + ', '.join(prof) + ')'

    def __str__(self):
        return self.name + ' ' + self.Profile()

    def Cost(self, speed, quality):
        sfactor = 1
        simpact = 0
        rending = 0
        wrange = self.range
        ap = dice_mean(self.armorPiercing)
        attacks = dice_mean(self.attacks)

        for s in self.weaponRules:
            if s == 'Deadly':
                sfactor *= 2.5
            elif s == 'Linked':
                quality -= 1
            elif s == 'Rending':
                # rending is 1/6 of having AP(8)
                rending = (1 / 6) * (ap_cost(8) - ap_cost(ap))
            elif s == 'Flux':
                # Flux is statistically like having quality +2
                quality -= 2
            elif s.startswith('Poison'):
                ap += int(s[7:-1]) / 2
            elif s.startswith('Blast'):
                sfactor *= int(s[6:-1])
            elif s.startswith('Impact'):
                simpact = int(s[7:-1])
            elif s == 'Autohit':
                quality = 1
            elif s == 'Limited':
                sfactor /= 2
            elif s == 'Sniper':
                # Sniper is 2+ hit and ignore cover (so statistically half an ap)
                quality = 2
                ap += 0.5
            elif s == 'Indirect':
                wrange *= 1.4
            elif s == 'Anti-Air':
                sfactor *= 1.10

        cost = sfactor * attacks * range_cost(wrange, speed) * (ap_cost(ap) * quality_attack_factor(quality) + rending)
        # Impact weapon have automatic hit, but only when charging (so 0.5 cost of the same weapon without quality factor)
        if simpact:
            cost += 0.5 * simpact * sfactor * ap_cost(ap) * range_cost(wrange, speed)

        return cost * adjust_attack_cost


class ComboWeapon():
    """ Combo weapons are weapon with multiple firing mode
        But you can fire only one of them per turn"""
    def __init__(self, name, profile, weapon):
        self.name = name
        self.profile = {profile: weapon}
        self.specialRules = []

    def Profile(self):
        return ' / '.join([profile + ' ' + weapon.Profile() for profile, weapon in self.profile.items()])

    def __str__(self):
        return self.name + ' ' + self.Profile()

    def AddProfile(self, profile, weapon):
        if profile in self.profile:
            msg = "profile {} already defined for {}".format(profile, self.name)
            raise ValueError(msg)
        self.profile[profile] = weapon

    def Cost(self, speed, quality):
        costs = [weapon.Cost(speed, quality) for weapon in self.profile.values()]
        return max(costs) + 0.25 * (min(costs))


class Armory(dict):
    # Armory class is a dictionnary of all Weapons and WarGear for a faction.
    def __init__(self, *args):
        dict.__init__(self, args)

    # get one equipment from the armory.
    def getOne(self, name):
        if name in self:
            return self[name]

        if name.endswith('s'):
            singular = name[:-1]
            if singular in self:
                self[name] = copy.copy(self[singular])
                self[name].name = name
                return self[name]
        raise ValueError('equipment {0} Not found !'.format(name))

    # Return the list of equipments objects, from their names.
    # if the name start with "2x ", return twice the same object in the list.
    def get(self, names):
        for name in names:
            if ' ' in name:
                firstword, remaining = name.split(' ', 1)
                if firstword.endswith('x') and firstword[:-1].isdigit():
                    n = int(firstword[:-1])
                    position = names.index(name)
                    names.remove(name)
                    for i in range(n):
                        names.insert(position, remaining)

        return [self.getOne(name) for name in names]

    # Add an equipments to the armory
    # if it's a weapon, also add the Linked variant
    def add(self, equipments):
        for equipment in equipments:
            if equipment.name in self:
                msg = 'Error {} is defined twice'.format(equipment.name)
                raise ValueError(msg)

            self[equipment.name] = equipment

            if isinstance(equipment, Weapon):
                if equipment.range > 0 and 'Linked' not in equipment.specialRules:
                    lname = 'Linked ' + equipment.name
                    self[lname] = Weapon(lname, equipment.range, equipment.attacks, equipment.armorPiercing, ['Linked'] + equipment.weaponRules)

                if '@' in equipment.name:
                    name, profile = equipment.name.split('@')
                    clname = 'Linked ' + name
                    if name in self:
                        self[name].AddProfile(profile, equipment)
                        self[clname].AddProfile(profile, self[lname])
                    else:
                        self[name] = ComboWeapon(name, profile, equipment)
                        self[clname] = ComboWeapon(name, profile, self[lname])


class Model:
    def __init__(self, name='Unknown Unit', speed=6, quality=4, defense=2, wounds=1, equipments=[], special=[]):
        self.name = name
        self.basespeed = speed
        self.quality = quality
        self.basedefense = defense
        self.basewounds = wounds
        self.equipments = equipments
        self.specialRules = special
        self.factionCost = 0
        self.Update()

    def __str__(self):
        attack, defense, misc, faction = self.ComputeCost()
        pretty = '{} {:.1f} pts\n\t'.format(self.name, sum([attack, defense, misc, faction]))
        for w in self.equipments:
            pretty += str(w) + '\n\t'

        pretty += ', '.join(self.specialRules)
        pretty += '\n\t'
        pretty += 'Defense {0:.1f} pts, Attack {1:.1f} pts, Other {2:.1f} pts Faction {3:.1f} pts\n'.format(attack, defense, misc, faction)
        return pretty

    def __copy__(self):
        return Model(self.name, self.basespeed, self.quality, self.basedefense, self.basewounds, self.equipments.copy(), self.specialRules.copy())

    @classmethod
    def from_dict(self, data, armory):
        data['equipments'] = armory.get(data.pop('equipment'))
        return self(**data)

    def Update(self):
        self.wargearSp = [sp for equ in self.equipments for sp in equ.specialRules]
        self.parseSpecialRules()

    def AddEquipments(self, equipments):
        self.equipments += equipments

    def RemoveEquipment(self, e):
        if e in self.equipments:
            self.equipments.remove(e)
            return

        if e.name.endswith('s'):
            singular = e.name[:-1]
            for equ in self.equipments:
                if singular == equ.name:
                    self.equipments.remove(equ)
                    return

        print("ERROR unit {0}, '{1}' not in current equipments '{2}'".format(self.name, e, self.equipments))

    def RemoveEquipments(self, equipments):
        for e in equipments:
            self.RemoveEquipment(e)

    def SetFactionCost(self, cost):
        self.factionCost = cost

    def AttackCost(self):
        return sum([w.Cost(self.speed, self.attackQuality) for w in self.equipments + self.spEquipments])

    def DefenseCost(self):
        defenseCost = quality_defense_factor(self.defenseQuality) * defense_cost(self.defense) * self.wounds
        # include speed to defense cost. hardened target which move fast are critical to control objectives.
        defenseCost *= (self.speed + 12) / (18)
        return defenseCost * adjust_defense_cost

    def ComputeCost(self):
        self.Update()
        atCost = self.AttackCost()
        dfCost = self.DefenseCost()
        otCost = self.globalAdd
        # For transporter, the cost depends on defense, and speed
        otCost += self.passengers * (dfCost / 150) * (self.speed / 6)
        otCost += (atCost + dfCost) * self.globalMultiplier
        return atCost, dfCost, otCost, self.factionCost

    def Cost(self):
        return sum(self.ComputeCost())

    def parseSpecialRules(self):
        self.speed = self.basespeed
        self.globalAdd = 0
        self.globalMultiplier = 0
        self.wounds = self.basewounds
        self.defense = self.basedefense
        self.spEquipments = []
        self.passengers = 0
        self.attackQuality = self.quality
        self.defenseQuality = self.quality

        specialRules = self.specialRules + self.wargearSp

        for s in specialRules:
            if s.startswith('Wounds('):
                self.wounds = int(s[6:-1])
            if s.startswith('Wounds+'):
                self.wounds += int(s[6:-1])
            elif s.startswith('Transport('):
                self.passengers = int(s[10:-1])
            elif s.startswith('Transport+'):
                self.passengers += int(s[10:])
            elif s.startswith('Psychic('):
                self.globalAdd += int(s[8:-1]) * 7
            elif s.startswith('Psychic+'):
                self.globalAdd += int(s[8:]) * 7
            elif s.startswith('Defense+'):
                self.defense += int(s[8:])
            elif s.startswith('Speed('):
                self.speed = int(s[6:-1])
            elif s.startswith('Speed+'):
                self.speed += int(s[6:-1])

        if 'Vehicle' in specialRules or 'Monster' in specialRules:
            smallStomp = Weapon('Monster Stomp', special=['Impact(3)'])
            self.spEquipments.append(smallStomp)
            if 'Fear' not in specialRules:
                specialRules.append('Fear')

        if 'Titan' in specialRules:
            titanStomp = Weapon('Titan Stomp', 0, 6, 2, ['Autohit'])
            self.spEquipments.append(titanStomp)
            if 'Fear' not in specialRules:
                specialRules.append('Fear')

        if 'Stealth' in specialRules:
            # Stealth is like +0.5 def, because it works only against ranged attack
            self.defense += 0.5
        if 'Good Shot' in specialRules:
            self.attackQuality = 4
        if 'Bad Shot' in specialRules:
            self.attackQuality = 5
        if 'Furious' in specialRules:
            self.globalAdd = 1
        if 'Fearless' in specialRules:
            self.defenseQuality -= 1

        if 'Ambush' in specialRules:
            if 'Scout' in specialRules:
                # Ambush and scout doesn't stack, since you can't use both
                self.globalMultiplier += 0.2
            else:
                self.globalMultiplier += 0.10
        if 'Scout' in specialRules:
            self.globalMultiplier += 0.15
        if 'Beacon' in specialRules:
            self.globalAdd += 10
        if 'Fear' in specialRules:
            self.globalAdd += 5
        if 'Strider' in specialRules:
            self.speed *= 1.2
        if 'Flying' in specialRules:
            self.speed *= 1.3
        # Flyers moves 36" but only in straight line.
        if 'Flyer' in specialRules:
            self.speed = 12

        if 'Regeneration' in specialRules:
            self.wounds *= 4 / 3


class Unit:
    def __init__(self, model, count):
        self.model = model
        self.count = count
        self.upgrades = []

    def __str__(self):
        s = 'Unit cost {:.1f} pts\n'.format(self.Cost())
        return s + '[{}] {}'.format(self.count, self.model)

    @classmethod
    def from_dict(self, data, armory):
        count = data.pop('count', 1)
        model = Model.from_dict(data, armory)
        return self(model, count)

    # Return unit name and count if more than one
    def PrettyName(self):
        if self.count > 1:
            return self.model.name + ' [{0}]'.format(self.count)
        return self.model.name

    def PrettyStats(self):
        model = self.model
        return [self.PrettyName(), str(model.basespeed) + '"', str(model.quality),
                str(model.basedefense) + '+', str(model.basewounds)]

    def Cost(self):
        return round(self.count * self.model.Cost())


def main():
    def pretty_weapon(weapon, speed, quality):
        print('{:.1f} pts\t\t {}'.format(weapon.Cost(speed, quality), weapon))

    pretty_weapon(Weapon('Flamethrower', 12, 6, 0), 12, 4)
    pretty_weapon(Weapon('Gatling', 18, 4, 1), 12, 4)
    pretty_weapon(Weapon('Pulse Rifle', 30, 1, 1), 12, 4)
    pretty_weapon(Weapon('Plasma', 24, 1, 3, ''), 12, 4)
    pretty_weapon(Weapon('Fusion Carbine', 18, 1, 7, ['Deadly']), 12, 4)
    pretty_weapon(Weapon('Railgun', 48, 1, 4, ['Linked', 'Deadly']), 12, 4)
    pretty_weapon(Weapon('Vehicle', special=['Impact(3)']), 12, 4)

    pulserifle = Weapon('Pulse Rifle', 30, 1, 1)

    gundrone = WarGear('Gun Drone', ["Regeneration"], [pulserifle])
    print(gundrone)

    grunt = Model('Grunt', 5, 4, [pulserifle, gundrone], ['Good Shot'])
    print(grunt)
    grunt_squad = Unit(grunt, 5)
    print(grunt_squad)

    grunt_cpt = Model('Grunt_cpt', 4, 4, [pulserifle], ['Tough(3)', 'Hero', 'Volley Fire'])
    print(grunt_cpt)

    suitfist = Weapon('Suit Fist', 0, 4, 1)
    battlesuit_cpt = Model('Battlesuit Captain', 3, 6, [suitfist], ['Ambush', 'Flying', 'Hero', 'Tough(3)'])
    print(battlesuit_cpt)


if __name__ == "__main__":
    # execute only if run as a script
    main()
